package nl.developerbas.hrscanner;

import android.app.ActionBar;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class SetPreferenceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar b = getActionBar();
        b.setTitle(getString(R.string.action_settings));

        getFragmentManager().beginTransaction().replace(android.R.id.content,
                new PrefsFragment()).commit();
    }

}
