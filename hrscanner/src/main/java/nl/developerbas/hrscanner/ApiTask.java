package nl.developerbas.hrscanner;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

//1 doinBackGround
//2 update
//3 onpostExecute
public class ApiTask extends AsyncTask<Void, Void, ApiResult> {

    private MainActivity context;

    private HttpURLConnection connection;

    private ApiRequest apiRequestParams;

    public ApiTask(MainActivity c, ApiRequest r) {
        context = c;
        apiRequestParams = r;
    }

    @Override
    protected ApiResult doInBackground(Void... voids) {

        try {

            Log.d("HRscanner", "starting checkin");
            URL url = new URL("http://www.developerbas.nl/hrscanner-api/index.php" + apiRequestParams.getQueryString());

            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000);
            connection.setConnectTimeout(5000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            connection.connect();

            BufferedReader reader = new BufferedReader(
                new InputStreamReader(connection.getInputStream(), "UTF-8")
            );

            String payload = reader.readLine();
            reader.close();


            JSONObject jsonObject = new JSONObject(payload);
            Log.d("HRScanner", jsonObject.toString());


            Boolean data = jsonObject.getBoolean("checkedIn");

            ApiResult apiResult = new ApiResult();
            apiResult.checkedIn = data;

            return apiResult;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
//            connection.disconnect();
        }

        return null;
    }

    @Override
    protected void onPostExecute(ApiResult data) {
        //Context.dostuff
        Log.d("Hrscanner", data.toString());

        if (MainActivity.dialog != null) {
            MainActivity.dialog.dismiss();
        }
        if (data.checkedIn == true) {
            new AlertDialog.Builder(context)
                    .setTitle("Welcome")
                    .setMessage("You succelfully checked in")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .show();
        } else {
            new AlertDialog.Builder(context)
                    .setTitle("Sorry!")
                    .setMessage("Checking in failed; please try again.")
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        }

    }
}
