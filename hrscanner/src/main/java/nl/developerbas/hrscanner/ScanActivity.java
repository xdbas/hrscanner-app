package nl.developerbas.hrscanner;

import android.app.Activity;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.IsoDep;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class ScanActivity extends Activity implements NfcAdapter.ReaderCallback {

    private static String TAG = "HRScanner";

    private NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableReaderMode(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter.enableReaderMode(this, this, NfcAdapter.FLAG_READER_NFC_A, null);
    }

    public void onTagDiscovered(Tag tag) {
        Log.d(TAG, "Tag Found");

        Ndef nDef = Ndef.get(tag);
        Log.d(TAG, nDef.toString());
        Toast.makeText(MainActivity.sContext, "Tag found", Toast.LENGTH_SHORT).show();
    }
}