package nl.developerbas.hrscanner;

/**
 * Created by basvanmanen on 17-04-14.
 */
public class ApiRequest {
    public String getStudentCode() {
        return studentCode;
    }

    public String getStudentClass() {
        return studentClass;
    }

    public String getClassCode() {
        return classCode;
    }

    private String studentCode;
    private String studentClass;

    private String classCode;

    public ApiRequest(String studentCode, String studentClass, String classCode) {
        this.studentCode = studentCode;
        this.studentClass = studentClass;
        this.classCode = classCode;
    }

    public String getQueryString()
    {
        String queryString = "?studentCode="  + this.getStudentCode() + "&studentClass=" + this.getStudentClass() + "&classCode=WN.04.022";

        return queryString;
    }
}
